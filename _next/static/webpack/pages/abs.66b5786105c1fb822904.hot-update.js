webpackHotUpdate_N_E("pages/abs",{

/***/ "./pages/abs.js":
/*!**********************!*\
  !*** ./pages/abs.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Abs; });
/* harmony import */ var C_Users_jansu_Documents_git_covid_generator_yarn_cache_babel_runtime_npm_7_11_2_f2cfabd212_2f127ad60a_zip_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./.yarn/cache/@babel-runtime-npm-7.11.2-f2cfabd212-2f127ad60a.zip/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./.yarn/cache/@babel-runtime-npm-7.11.2-f2cfabd212-2f127ad60a.zip/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_jansu_Documents_git_covid_generator_yarn_cache_babel_runtime_npm_7_11_2_f2cfabd212_2f127ad60a_zip_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./.yarn/cache/@babel-runtime-npm-7.11.2-f2cfabd212-2f127ad60a.zip/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./.yarn/cache/@babel-runtime-npm-7.11.2-f2cfabd212-2f127ad60a.zip/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./.yarn/cache/react-npm-17.0.1-98658812fc-a76d86ec97.zip/node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swr */ "./.yarn/$$virtual/swr-virtual-9d1a62bdbc/0/cache/swr-npm-0.3.5-4657b13232-c0747ebb2e.zip/node_modules/swr/esm/index.js");
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-table */ "./.yarn/$$virtual/react-table-virtual-a7101b8eff/0/cache/react-table-npm-7.6.0-511df9e0e7-fbcc429478.zip/node_modules/react-table/index.js");
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_table__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./.yarn/cache/react-npm-17.0.1-98658812fc-a76d86ec97.zip/node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! date-fns */ "./.yarn/cache/date-fns-npm-2.16.1-2f082a8b35-fbe5d9aa1d.zip/node_modules/date-fns/esm/index.js");
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../styles/Table.module.css */ "./styles/Table.module.css");
/* harmony import */ var _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8__);





var _jsxFileName = "C:\\Users\\jansu\\Documents\\git\\covid-generator\\pages\\abs.js",
    _s = $RefreshSig$(),
    _s2 = $RefreshSig$(),
    _s3 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_jansu_Documents_git_covid_generator_yarn_cache_babel_runtime_npm_7_11_2_f2cfabd212_2f127ad60a_zip_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }







function Abs() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])(_components_Layout__WEBPACK_IMPORTED_MODULE_7__["default"], {
    title: "Absolutn\xED p\u0159\xEDrustky",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])(AbsoluteIncrease, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 5
  }, this);
}
_c = Abs;

function AbsoluteIncrease() {
  _s();

  var _useSWR = Object(swr__WEBPACK_IMPORTED_MODULE_3__["default"])('https://covid.suchomel.ovh/api/absolute_increase.json', {
    refreshInterval: 60000
  }),
      absData = _useSWR.data,
      absError = _useSWR.error;

  if (absError) return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("div", {
    children: "nepoda\u0159ilo se na\u010D\xEDst data"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 23,
    columnNumber: 24
  }, this);
  if (!absData) return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("div", {
    children: "na\u010D\xEDt\xE1n\xED..."
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 24,
    columnNumber: 24
  }, this);
  var tableData = Object.entries(absData['cz_increase']).map(function (_ref) {
    var _ref2 = Object(C_Users_jansu_Documents_git_covid_generator_yarn_cache_babel_runtime_npm_7_11_2_f2cfabd212_2f127ad60a_zip_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_ref, 2),
        statDate = _ref2[0],
        cz = _ref2[1];

    return {
      date: Object(date_fns__WEBPACK_IMPORTED_MODULE_6__["format"])(new Date(+statDate), 'yyyy-MM-dd'),
      cz: cz,
      sk: +absData['sk_increase'][statDate],
      at: absData['at_increase'][statDate],
      de: absData['de_increase'][statDate],
      pl: absData['pl_increase'][statDate],
      fr: absData['fr_increase'][statDate],
      es: absData['es_increase'][statDate],
      gb: absData['gb_increase'][statDate],
      nl: absData['nl_increase'][statDate],
      be: absData['be_increase'][statDate],
      it: absData['it_increase'][statDate],
      sl: absData['sl_increase'][statDate],
      ch: absData['ch_increase'][statDate]
    };
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])(AbsoluteIncreaseTable, {
    tableData: tableData
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 44,
    columnNumber: 10
  }, this);
}

_s(AbsoluteIncrease, "dB6Y7fHaANNu/vDjv6vElpL8E9Y=", false, function () {
  return [swr__WEBPACK_IMPORTED_MODULE_3__["default"]];
});

_c2 = AbsoluteIncrease;

function AbsoluteIncreaseTable(_ref3) {
  _s2();

  var tableData = _ref3.tableData;
  var data = react__WEBPACK_IMPORTED_MODULE_5___default.a.useMemo(function () {
    return tableData;
  }, []);
  var columns = react__WEBPACK_IMPORTED_MODULE_5___default.a.useMemo(function () {
    return [{
      Header: 'Date',
      accessor: 'date'
    }, {
      Header: 'Czechia',
      accessor: 'cz'
    }, {
      Header: 'Slovakia',
      accessor: 'sk'
    }, {
      Header: 'Austria',
      accessor: 'at'
    }, {
      Header: 'Germany',
      accessor: 'de'
    }, {
      Header: 'Poland',
      accessor: 'pl'
    }, {
      Header: 'France',
      accessor: 'fr'
    }, {
      Header: 'Spain',
      accessor: 'es'
    }, {
      Header: 'Great Britain',
      accessor: 'gb'
    }, {
      Header: 'Netherlands',
      accessor: 'nl'
    }, {
      Header: 'Belgium',
      accessor: 'be'
    }, {
      Header: 'Italy',
      accessor: 'it'
    }, {
      Header: 'Slovenia',
      accessor: 'sl'
    }, {
      Header: 'Switzerland',
      accessor: 'ch'
    }];
  }, []);
  var sortBy = react__WEBPACK_IMPORTED_MODULE_5___default.a.useMemo(function () {
    return [{
      id: 'date',
      desc: true
    }];
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])(Table, {
      data: data,
      columns: columns,
      sortBy: sortBy
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 5
    }, this)
  }, void 0, false);
}

_s2(AbsoluteIncreaseTable, "9z4WNfDTDhTNcZzt3zRLJgA1aJY=");

_c3 = AbsoluteIncreaseTable;

function Table(_ref4) {
  _s3();

  var _this = this;

  var data = _ref4.data,
      columns = _ref4.columns,
      sortBy = _ref4.sortBy;
  var tableInstance = Object(react_table__WEBPACK_IMPORTED_MODULE_4__["useTable"])({
    columns: columns,
    data: data,
    initialState: {
      sortBy: sortBy
    }
  }, react_table__WEBPACK_IMPORTED_MODULE_4__["useSortBy"]);
  var getTableProps = tableInstance.getTableProps,
      getTableBodyProps = tableInstance.getTableBodyProps,
      headerGroups = tableInstance.headerGroups,
      rows = tableInstance.rows,
      prepareRow = tableInstance.prepareRow;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("table", _objectSpread(_objectSpread({}, getTableProps()), {}, {
    className: _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.table,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("thead", {
      children: // Loop over the header rows
      headerGroups.map(function (headerGroup) {
        return (
          /*#__PURE__*/
          // Apply the header row props
          Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("tr", _objectSpread(_objectSpread({}, headerGroup.getHeaderGroupProps()), {}, {
            children: // Loop over the headers in each row
            headerGroup.headers.map(function (column) {
              return (
                /*#__PURE__*/
                // Apply the header cell props
                Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("th", _objectSpread(_objectSpread({}, column.getHeaderProps(column.getSortByToggleProps())), {}, {
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("span", {
                    children: column.isSorted ? column.isSortedDesc ? 'v ' : 'ᴧ ' : ''
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 141,
                    columnNumber: 19
                  }, _this), column.render('Header')]
                }), void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 140,
                  columnNumber: 17
                }, _this)
              );
            })
          }), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 136,
            columnNumber: 11
          }, _this)
        );
      })
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 132,
      columnNumber: 5
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("tbody", _objectSpread(_objectSpread({}, getTableBodyProps()), {}, {
      children: // Loop over the table rows
      rows.map(function (row) {
        // Prepare the row for display
        prepareRow(row);
        return (
          /*#__PURE__*/
          // Apply the row props
          Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("tr", _objectSpread(_objectSpread({}, row.getRowProps()), {}, {
            children: // Loop over the rows cells
            row.cells.map(function (cell) {
              // Apply the cell props
              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_2__["jsxDEV"])("td", _objectSpread(_objectSpread({}, cell.getCellProps()), {}, {
                children: // Render the cell contents
                cell.render('Cell')
              }), void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 167,
                columnNumber: 21
              }, _this);
            })
          }), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 162,
            columnNumber: 13
          }, _this)
        );
      })
    }), void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 5
    }, this)]
  }), void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 131,
    columnNumber: 10
  }, this);
}

_s3(Table, "Tx/KrHBxU/jU8U+MIvn5eB/AsMY=", false, function () {
  return [react_table__WEBPACK_IMPORTED_MODULE_4__["useTable"]];
});

_c4 = Table;

function getRowStyle(dayOfWeek) {
  if (dayOfWeek == 1) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_monday;
  } else if (dayOfWeek == 2) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_tuesday;
  } else if (dayOfWeek == 3) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_wednesday;
  } else if (dayOfWeek == 4) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_thursday;
  } else if (dayOfWeek == 5) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_friday;
  } else if (dayOfWeek == 6) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_saturday;
  } else if (dayOfWeek == 7) {
    return _styles_Table_module_css__WEBPACK_IMPORTED_MODULE_8___default.a.row_sunday;
  }
}

var _c, _c2, _c3, _c4;

$RefreshReg$(_c, "Abs");
$RefreshReg$(_c2, "AbsoluteIncrease");
$RefreshReg$(_c3, "AbsoluteIncreaseTable");
$RefreshReg$(_c4, "Table");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../.yarn/$$virtual/webpack-virtual-8c4a3f47df/0/cache/webpack-npm-4.44.1-977bccfd33-d4d140010b.zip/node_modules/webpack/buildin/harmony-module.js */ "./.yarn/$$virtual/webpack-virtual-8c4a3f47df/0/cache/webpack-npm-4.44.1-977bccfd33-d4d140010b.zip/node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvYWJzLmpzIl0sIm5hbWVzIjpbIkFicyIsIkFic29sdXRlSW5jcmVhc2UiLCJ1c2VTV1IiLCJyZWZyZXNoSW50ZXJ2YWwiLCJhYnNEYXRhIiwiZGF0YSIsImFic0Vycm9yIiwiZXJyb3IiLCJ0YWJsZURhdGEiLCJPYmplY3QiLCJlbnRyaWVzIiwibWFwIiwic3RhdERhdGUiLCJjeiIsImRhdGUiLCJmb3JtYXQiLCJEYXRlIiwic2siLCJhdCIsImRlIiwicGwiLCJmciIsImVzIiwiZ2IiLCJubCIsImJlIiwiaXQiLCJzbCIsImNoIiwiQWJzb2x1dGVJbmNyZWFzZVRhYmxlIiwiUmVhY3QiLCJ1c2VNZW1vIiwiY29sdW1ucyIsIkhlYWRlciIsImFjY2Vzc29yIiwic29ydEJ5IiwiaWQiLCJkZXNjIiwiVGFibGUiLCJ0YWJsZUluc3RhbmNlIiwidXNlVGFibGUiLCJpbml0aWFsU3RhdGUiLCJ1c2VTb3J0QnkiLCJnZXRUYWJsZVByb3BzIiwiZ2V0VGFibGVCb2R5UHJvcHMiLCJoZWFkZXJHcm91cHMiLCJyb3dzIiwicHJlcGFyZVJvdyIsInRhYmxlU3R5bGVzIiwidGFibGUiLCJoZWFkZXJHcm91cCIsImdldEhlYWRlckdyb3VwUHJvcHMiLCJoZWFkZXJzIiwiY29sdW1uIiwiZ2V0SGVhZGVyUHJvcHMiLCJnZXRTb3J0QnlUb2dnbGVQcm9wcyIsImlzU29ydGVkIiwiaXNTb3J0ZWREZXNjIiwicmVuZGVyIiwicm93IiwiZ2V0Um93UHJvcHMiLCJjZWxscyIsImNlbGwiLCJnZXRDZWxsUHJvcHMiLCJnZXRSb3dTdHlsZSIsImRheU9mV2VlayIsInJvd19tb25kYXkiLCJyb3dfdHVlc2RheSIsInJvd193ZWRuZXNkYXkiLCJyb3dfdGh1cnNkYXkiLCJyb3dfZnJpZGF5Iiwicm93X3NhdHVyZGF5Iiwicm93X3N1bmRheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBR2UsU0FBU0EsR0FBVCxHQUFlO0FBQzVCLHNCQUNFLHFFQUFDLDBEQUFEO0FBQVEsU0FBSyxFQUFDLGdDQUFkO0FBQUEsMkJBQ0UscUVBQUMsZ0JBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQUtEO0tBTnVCQSxHOztBQVF4QixTQUFTQyxnQkFBVCxHQUE0QjtBQUFBOztBQUFBLGdCQUNpQkMsbURBQU0sQ0FBQyx1REFBRCxFQUEwRDtBQUFFQyxtQkFBZSxFQUFFO0FBQW5CLEdBQTFELENBRHZCO0FBQUEsTUFDWkMsT0FEWSxXQUNsQkMsSUFEa0I7QUFBQSxNQUNJQyxRQURKLFdBQ0hDLEtBREc7O0FBRzFCLE1BQUlELFFBQUosRUFBYyxvQkFBTztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFQO0FBQ2QsTUFBSSxDQUFDRixPQUFMLEVBQWMsb0JBQU87QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUDtBQUVkLE1BQUlJLFNBQVMsR0FBR0MsTUFBTSxDQUFDQyxPQUFQLENBQWVOLE9BQU8sQ0FBQyxhQUFELENBQXRCLEVBQ2JPLEdBRGEsQ0FDVDtBQUFBO0FBQUEsUUFBRUMsUUFBRjtBQUFBLFFBQVlDLEVBQVo7O0FBQUEsV0FBcUI7QUFDeEJDLFVBQUksRUFBRUMsdURBQU0sQ0FBQyxJQUFJQyxJQUFKLENBQVMsQ0FBQ0osUUFBVixDQUFELEVBQXNCLFlBQXRCLENBRFk7QUFFeEJDLFFBQUUsRUFBRkEsRUFGd0I7QUFHeEJJLFFBQUUsRUFBRSxDQUFDYixPQUFPLENBQUMsYUFBRCxDQUFQLENBQXVCUSxRQUF2QixDQUhtQjtBQUl4Qk0sUUFBRSxFQUFFZCxPQUFPLENBQUMsYUFBRCxDQUFQLENBQXVCUSxRQUF2QixDQUpvQjtBQUt4Qk8sUUFBRSxFQUFFZixPQUFPLENBQUMsYUFBRCxDQUFQLENBQXVCUSxRQUF2QixDQUxvQjtBQU14QlEsUUFBRSxFQUFFaEIsT0FBTyxDQUFDLGFBQUQsQ0FBUCxDQUF1QlEsUUFBdkIsQ0FOb0I7QUFPeEJTLFFBQUUsRUFBRWpCLE9BQU8sQ0FBQyxhQUFELENBQVAsQ0FBdUJRLFFBQXZCLENBUG9CO0FBUXhCVSxRQUFFLEVBQUVsQixPQUFPLENBQUMsYUFBRCxDQUFQLENBQXVCUSxRQUF2QixDQVJvQjtBQVN4QlcsUUFBRSxFQUFFbkIsT0FBTyxDQUFDLGFBQUQsQ0FBUCxDQUF1QlEsUUFBdkIsQ0FUb0I7QUFVeEJZLFFBQUUsRUFBRXBCLE9BQU8sQ0FBQyxhQUFELENBQVAsQ0FBdUJRLFFBQXZCLENBVm9CO0FBV3hCYSxRQUFFLEVBQUVyQixPQUFPLENBQUMsYUFBRCxDQUFQLENBQXVCUSxRQUF2QixDQVhvQjtBQVl4QmMsUUFBRSxFQUFFdEIsT0FBTyxDQUFDLGFBQUQsQ0FBUCxDQUF1QlEsUUFBdkIsQ0Fab0I7QUFheEJlLFFBQUUsRUFBRXZCLE9BQU8sQ0FBQyxhQUFELENBQVAsQ0FBdUJRLFFBQXZCLENBYm9CO0FBY3hCZ0IsUUFBRSxFQUFFeEIsT0FBTyxDQUFDLGFBQUQsQ0FBUCxDQUF1QlEsUUFBdkI7QUFkb0IsS0FBckI7QUFBQSxHQURTLENBQWhCO0FBa0JBLHNCQUFPLHFFQUFDLHFCQUFEO0FBQXVCLGFBQVMsRUFBRUo7QUFBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFQO0FBQ0Q7O0dBekJRUCxnQjtVQUNvQ0MsMkM7OztNQURwQ0QsZ0I7O0FBMkJULFNBQVM0QixxQkFBVCxRQUE4QztBQUFBOztBQUFBLE1BQWJyQixTQUFhLFNBQWJBLFNBQWE7QUFDNUMsTUFBTUgsSUFBSSxHQUFHeUIsNENBQUssQ0FBQ0MsT0FBTixDQUFjO0FBQUEsV0FBTXZCLFNBQU47QUFBQSxHQUFkLEVBQStCLEVBQS9CLENBQWI7QUFFQSxNQUFNd0IsT0FBTyxHQUFHRiw0Q0FBSyxDQUFDQyxPQUFOLENBQ2Q7QUFBQSxXQUFNLENBQ0o7QUFDRUUsWUFBTSxFQUFFLE1BRFY7QUFFRUMsY0FBUSxFQUFFO0FBRlosS0FESSxFQUtKO0FBQ0VELFlBQU0sRUFBRSxTQURWO0FBRUVDLGNBQVEsRUFBRTtBQUZaLEtBTEksRUFTSjtBQUNFRCxZQUFNLEVBQUUsVUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQVRJLEVBYUo7QUFDRUQsWUFBTSxFQUFFLFNBRFY7QUFFRUMsY0FBUSxFQUFFO0FBRlosS0FiSSxFQWlCSjtBQUNFRCxZQUFNLEVBQUUsU0FEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQWpCSSxFQXFCSjtBQUNFRCxZQUFNLEVBQUUsUUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQXJCSSxFQXlCSjtBQUNFRCxZQUFNLEVBQUUsUUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQXpCSSxFQTZCSjtBQUNFRCxZQUFNLEVBQUUsT0FEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQTdCSSxFQWlDSjtBQUNFRCxZQUFNLEVBQUUsZUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQWpDSSxFQXFDSjtBQUNFRCxZQUFNLEVBQUUsYUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQXJDSSxFQXlDSjtBQUNFRCxZQUFNLEVBQUUsU0FEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQXpDSSxFQTZDSjtBQUNFRCxZQUFNLEVBQUUsT0FEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQTdDSSxFQWlESjtBQUNFRCxZQUFNLEVBQUUsVUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQWpESSxFQXFESjtBQUNFRCxZQUFNLEVBQUUsYUFEVjtBQUVFQyxjQUFRLEVBQUU7QUFGWixLQXJESSxDQUFOO0FBQUEsR0FEYyxFQTJEZCxFQTNEYyxDQUFoQjtBQThEQSxNQUFNQyxNQUFNLEdBQUdMLDRDQUFLLENBQUNDLE9BQU4sQ0FBYztBQUFBLFdBQU0sQ0FBQztBQUFFSyxRQUFFLEVBQUUsTUFBTjtBQUFjQyxVQUFJLEVBQUU7QUFBcEIsS0FBRCxDQUFOO0FBQUEsR0FBZCxFQUFrRCxFQUFsRCxDQUFmO0FBRUEsc0JBQU87QUFBQSwyQkFDTCxxRUFBQyxLQUFEO0FBQU8sVUFBSSxFQUFFaEMsSUFBYjtBQUFtQixhQUFPLEVBQUUyQixPQUE1QjtBQUFxQyxZQUFNLEVBQUVHO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESyxtQkFBUDtBQUdEOztJQXRFUU4scUI7O01BQUFBLHFCOztBQXdFVCxTQUFTUyxLQUFULFFBQTBDO0FBQUE7O0FBQUE7O0FBQUEsTUFBekJqQyxJQUF5QixTQUF6QkEsSUFBeUI7QUFBQSxNQUFuQjJCLE9BQW1CLFNBQW5CQSxPQUFtQjtBQUFBLE1BQVZHLE1BQVUsU0FBVkEsTUFBVTtBQUN4QyxNQUFNSSxhQUFhLEdBQUdDLDREQUFRLENBQUM7QUFBRVIsV0FBTyxFQUFQQSxPQUFGO0FBQVczQixRQUFJLEVBQUpBLElBQVg7QUFBaUJvQyxnQkFBWSxFQUFFO0FBQUVOLFlBQU0sRUFBTkE7QUFBRjtBQUEvQixHQUFELEVBQThDTyxxREFBOUMsQ0FBOUI7QUFEd0MsTUFJdENDLGFBSnNDLEdBU3BDSixhQVRvQyxDQUl0Q0ksYUFKc0M7QUFBQSxNQUt0Q0MsaUJBTHNDLEdBU3BDTCxhQVRvQyxDQUt0Q0ssaUJBTHNDO0FBQUEsTUFNdENDLFlBTnNDLEdBU3BDTixhQVRvQyxDQU10Q00sWUFOc0M7QUFBQSxNQU90Q0MsSUFQc0MsR0FTcENQLGFBVG9DLENBT3RDTyxJQVBzQztBQUFBLE1BUXRDQyxVQVJzQyxHQVNwQ1IsYUFUb0MsQ0FRdENRLFVBUnNDO0FBWXhDLHNCQUFPLDhHQUFXSixhQUFhLEVBQXhCO0FBQTRCLGFBQVMsRUFBRUssK0RBQVcsQ0FBQ0MsS0FBbkQ7QUFBQSw0QkFDTDtBQUFBLGdCQUNHO0FBQ0NKLGtCQUFZLENBQUNsQyxHQUFiLENBQWlCLFVBQUF1QyxXQUFXO0FBQUE7QUFBQTtBQUMxQjtBQUNBLHFIQUFRQSxXQUFXLENBQUNDLG1CQUFaLEVBQVI7QUFBQSxzQkFDRztBQUNDRCx1QkFBVyxDQUFDRSxPQUFaLENBQW9CekMsR0FBcEIsQ0FBd0IsVUFBQTBDLE1BQU07QUFBQTtBQUFBO0FBQzVCO0FBQ0EsMkhBQVFBLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQkQsTUFBTSxDQUFDRSxvQkFBUCxFQUF0QixDQUFSO0FBQUEsMENBQ0U7QUFBQSw4QkFDR0YsTUFBTSxDQUFDRyxRQUFQLEdBQ0dILE1BQU0sQ0FBQ0ksWUFBUCxHQUNFLElBREYsR0FFRSxJQUhMLEdBSUc7QUFMTjtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQURGLEVBUUdKLE1BQU0sQ0FBQ0ssTUFBUCxDQUFjLFFBQWQsQ0FSSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGNEI7QUFBQSxhQUE5QjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGMEI7QUFBQSxPQUE1QjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFESyxlQXdCTCw4R0FBV2QsaUJBQWlCLEVBQTVCO0FBQUEsZ0JBQ0c7QUFDQ0UsVUFBSSxDQUFDbkMsR0FBTCxDQUFTLFVBQUFnRCxHQUFHLEVBQUk7QUFDZDtBQUNBWixrQkFBVSxDQUFDWSxHQUFELENBQVY7QUFDQTtBQUFBO0FBQ0U7QUFDQSxxSEFBUUEsR0FBRyxDQUFDQyxXQUFKLEVBQVI7QUFBQSxzQkFDRztBQUNDRCxlQUFHLENBQUNFLEtBQUosQ0FBVWxELEdBQVYsQ0FBYyxVQUFBbUQsSUFBSSxFQUFJO0FBQ3BCO0FBQ0Esa0NBQ0UsMkdBQVFBLElBQUksQ0FBQ0MsWUFBTCxFQUFSO0FBQUEsMEJBQ0c7QUFDQ0Qsb0JBQUksQ0FBQ0osTUFBTCxDQUFZLE1BQVo7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQURGO0FBTUQsYUFSRDtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGRjtBQWVELE9BbEJEO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXhCSztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUDtBQStDRDs7SUEzRFFwQixLO1VBQ2VFLG9EOzs7TUFEZkYsSzs7QUE2RFQsU0FBUzBCLFdBQVQsQ0FBcUJDLFNBQXJCLEVBQWdDO0FBQzlCLE1BQUlBLFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUNsQixXQUFPakIsK0RBQVcsQ0FBQ2tCLFVBQW5CO0FBQ0QsR0FGRCxNQUVPLElBQUlELFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ21CLFdBQW5CO0FBQ0QsR0FGTSxNQUVBLElBQUlGLFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ29CLGFBQW5CO0FBQ0QsR0FGTSxNQUVBLElBQUlILFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ3FCLFlBQW5CO0FBQ0QsR0FGTSxNQUVBLElBQUlKLFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ3NCLFVBQW5CO0FBQ0QsR0FGTSxNQUVBLElBQUlMLFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ3VCLFlBQW5CO0FBQ0QsR0FGTSxNQUVBLElBQUlOLFNBQVMsSUFBSSxDQUFqQixFQUFvQjtBQUN6QixXQUFPakIsK0RBQVcsQ0FBQ3dCLFVBQW5CO0FBQ0Q7QUFDRiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9hYnMuNjZiNTc4NjEwNWMxZmI4MjI5MDQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB1c2VTV1IgZnJvbSAnc3dyJ1xyXG5pbXBvcnQgeyB1c2VUYWJsZSwgdXNlU29ydEJ5IH0gZnJvbSAncmVhY3QtdGFibGUnXHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcclxuXHJcbmltcG9ydCB7IGZvcm1hdCB9IGZyb20gJ2RhdGUtZm5zJ1xyXG5cclxuaW1wb3J0IExheW91dCBmcm9tIFwiLi4vY29tcG9uZW50cy9MYXlvdXRcIlxyXG5cclxuaW1wb3J0IHRhYmxlU3R5bGVzIGZyb20gJy4uL3N0eWxlcy9UYWJsZS5tb2R1bGUuY3NzJ1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEFicygpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPExheW91dCB0aXRsZT1cIkFic29sdXRuw60gcMWZw61ydXN0a3lcIj5cclxuICAgICAgPEFic29sdXRlSW5jcmVhc2UgLz5cclxuICAgIDwvTGF5b3V0PlxyXG4gIClcclxufVxyXG5cclxuZnVuY3Rpb24gQWJzb2x1dGVJbmNyZWFzZSgpIHtcclxuICBjb25zdCB7IGRhdGE6IGFic0RhdGEsIGVycm9yOiBhYnNFcnJvciB9ID0gdXNlU1dSKCdodHRwczovL2NvdmlkLnN1Y2hvbWVsLm92aC9hcGkvYWJzb2x1dGVfaW5jcmVhc2UuanNvbicsIHsgcmVmcmVzaEludGVydmFsOiA2MDAwMCB9KVxyXG5cclxuICBpZiAoYWJzRXJyb3IpIHJldHVybiA8ZGl2Pm5lcG9kYcWZaWxvIHNlIG5hxI3DrXN0IGRhdGE8L2Rpdj5cclxuICBpZiAoIWFic0RhdGEpIHJldHVybiA8ZGl2Pm5hxI3DrXTDoW7DrS4uLjwvZGl2PlxyXG5cclxuICBsZXQgdGFibGVEYXRhID0gT2JqZWN0LmVudHJpZXMoYWJzRGF0YVsnY3pfaW5jcmVhc2UnXSlcclxuICAgIC5tYXAoKFtzdGF0RGF0ZSwgY3pdKSA9PiAoe1xyXG4gICAgICBkYXRlOiBmb3JtYXQobmV3IERhdGUoK3N0YXREYXRlKSwgJ3l5eXktTU0tZGQnKSxcclxuICAgICAgY3osXHJcbiAgICAgIHNrOiArYWJzRGF0YVsnc2tfaW5jcmVhc2UnXVtzdGF0RGF0ZV0sXHJcbiAgICAgIGF0OiBhYnNEYXRhWydhdF9pbmNyZWFzZSddW3N0YXREYXRlXSxcclxuICAgICAgZGU6IGFic0RhdGFbJ2RlX2luY3JlYXNlJ11bc3RhdERhdGVdLFxyXG4gICAgICBwbDogYWJzRGF0YVsncGxfaW5jcmVhc2UnXVtzdGF0RGF0ZV0sXHJcbiAgICAgIGZyOiBhYnNEYXRhWydmcl9pbmNyZWFzZSddW3N0YXREYXRlXSxcclxuICAgICAgZXM6IGFic0RhdGFbJ2VzX2luY3JlYXNlJ11bc3RhdERhdGVdLFxyXG4gICAgICBnYjogYWJzRGF0YVsnZ2JfaW5jcmVhc2UnXVtzdGF0RGF0ZV0sXHJcbiAgICAgIG5sOiBhYnNEYXRhWydubF9pbmNyZWFzZSddW3N0YXREYXRlXSxcclxuICAgICAgYmU6IGFic0RhdGFbJ2JlX2luY3JlYXNlJ11bc3RhdERhdGVdLFxyXG4gICAgICBpdDogYWJzRGF0YVsnaXRfaW5jcmVhc2UnXVtzdGF0RGF0ZV0sXHJcbiAgICAgIHNsOiBhYnNEYXRhWydzbF9pbmNyZWFzZSddW3N0YXREYXRlXSxcclxuICAgICAgY2g6IGFic0RhdGFbJ2NoX2luY3JlYXNlJ11bc3RhdERhdGVdXHJcbiAgICB9KSk7XHJcblxyXG4gIHJldHVybiA8QWJzb2x1dGVJbmNyZWFzZVRhYmxlIHRhYmxlRGF0YT17dGFibGVEYXRhfSAvPlxyXG59XHJcblxyXG5mdW5jdGlvbiBBYnNvbHV0ZUluY3JlYXNlVGFibGUoeyB0YWJsZURhdGEgfSkge1xyXG4gIGNvbnN0IGRhdGEgPSBSZWFjdC51c2VNZW1vKCgpID0+IHRhYmxlRGF0YSwgW10pXHJcblxyXG4gIGNvbnN0IGNvbHVtbnMgPSBSZWFjdC51c2VNZW1vKFxyXG4gICAgKCkgPT4gW1xyXG4gICAgICB7XHJcbiAgICAgICAgSGVhZGVyOiAnRGF0ZScsXHJcbiAgICAgICAgYWNjZXNzb3I6ICdkYXRlJyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIEhlYWRlcjogJ0N6ZWNoaWEnLFxyXG4gICAgICAgIGFjY2Vzc29yOiAnY3onLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgSGVhZGVyOiAnU2xvdmFraWEnLFxyXG4gICAgICAgIGFjY2Vzc29yOiAnc2snLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgSGVhZGVyOiAnQXVzdHJpYScsXHJcbiAgICAgICAgYWNjZXNzb3I6ICdhdCcsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBIZWFkZXI6ICdHZXJtYW55JyxcclxuICAgICAgICBhY2Nlc3NvcjogJ2RlJyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIEhlYWRlcjogJ1BvbGFuZCcsXHJcbiAgICAgICAgYWNjZXNzb3I6ICdwbCcsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBIZWFkZXI6ICdGcmFuY2UnLFxyXG4gICAgICAgIGFjY2Vzc29yOiAnZnInLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgSGVhZGVyOiAnU3BhaW4nLFxyXG4gICAgICAgIGFjY2Vzc29yOiAnZXMnLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgSGVhZGVyOiAnR3JlYXQgQnJpdGFpbicsXHJcbiAgICAgICAgYWNjZXNzb3I6ICdnYicsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBIZWFkZXI6ICdOZXRoZXJsYW5kcycsXHJcbiAgICAgICAgYWNjZXNzb3I6ICdubCcsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBIZWFkZXI6ICdCZWxnaXVtJyxcclxuICAgICAgICBhY2Nlc3NvcjogJ2JlJyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIEhlYWRlcjogJ0l0YWx5JyxcclxuICAgICAgICBhY2Nlc3NvcjogJ2l0JyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIEhlYWRlcjogJ1Nsb3ZlbmlhJyxcclxuICAgICAgICBhY2Nlc3NvcjogJ3NsJyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIEhlYWRlcjogJ1N3aXR6ZXJsYW5kJyxcclxuICAgICAgICBhY2Nlc3NvcjogJ2NoJ1xyXG4gICAgICB9XHJcbiAgICBdLFxyXG4gICAgW11cclxuICApXHJcblxyXG4gIGNvbnN0IHNvcnRCeSA9IFJlYWN0LnVzZU1lbW8oKCkgPT4gW3sgaWQ6ICdkYXRlJywgZGVzYzogdHJ1ZSB9XSwgW10pXHJcblxyXG4gIHJldHVybiA8PlxyXG4gICAgPFRhYmxlIGRhdGE9e2RhdGF9IGNvbHVtbnM9e2NvbHVtbnN9IHNvcnRCeT17c29ydEJ5fSAvPlxyXG4gIDwvPlxyXG59XHJcblxyXG5mdW5jdGlvbiBUYWJsZSh7IGRhdGEsIGNvbHVtbnMsIHNvcnRCeSB9KSB7XHJcbiAgY29uc3QgdGFibGVJbnN0YW5jZSA9IHVzZVRhYmxlKHsgY29sdW1ucywgZGF0YSwgaW5pdGlhbFN0YXRlOiB7IHNvcnRCeSB9IH0sIHVzZVNvcnRCeSlcclxuXHJcbiAgY29uc3Qge1xyXG4gICAgZ2V0VGFibGVQcm9wcyxcclxuICAgIGdldFRhYmxlQm9keVByb3BzLFxyXG4gICAgaGVhZGVyR3JvdXBzLFxyXG4gICAgcm93cyxcclxuICAgIHByZXBhcmVSb3csXHJcbiAgfSA9IHRhYmxlSW5zdGFuY2VcclxuXHJcblxyXG4gIHJldHVybiA8dGFibGUgey4uLmdldFRhYmxlUHJvcHMoKX0gY2xhc3NOYW1lPXt0YWJsZVN0eWxlcy50YWJsZX0+XHJcbiAgICA8dGhlYWQ+XHJcbiAgICAgIHsvLyBMb29wIG92ZXIgdGhlIGhlYWRlciByb3dzXHJcbiAgICAgICAgaGVhZGVyR3JvdXBzLm1hcChoZWFkZXJHcm91cCA9PiAoXHJcbiAgICAgICAgICAvLyBBcHBseSB0aGUgaGVhZGVyIHJvdyBwcm9wc1xyXG4gICAgICAgICAgPHRyIHsuLi5oZWFkZXJHcm91cC5nZXRIZWFkZXJHcm91cFByb3BzKCl9PlxyXG4gICAgICAgICAgICB7Ly8gTG9vcCBvdmVyIHRoZSBoZWFkZXJzIGluIGVhY2ggcm93XHJcbiAgICAgICAgICAgICAgaGVhZGVyR3JvdXAuaGVhZGVycy5tYXAoY29sdW1uID0+IChcclxuICAgICAgICAgICAgICAgIC8vIEFwcGx5IHRoZSBoZWFkZXIgY2VsbCBwcm9wc1xyXG4gICAgICAgICAgICAgICAgPHRoIHsuLi5jb2x1bW4uZ2V0SGVhZGVyUHJvcHMoY29sdW1uLmdldFNvcnRCeVRvZ2dsZVByb3BzKCkpfT5cclxuICAgICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAge2NvbHVtbi5pc1NvcnRlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgPyBjb2x1bW4uaXNTb3J0ZWREZXNjXHJcbiAgICAgICAgICAgICAgICAgICAgICAgID8gJ3YgJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA6ICfhtKcgJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgOiAnJ31cclxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICB7Y29sdW1uLnJlbmRlcignSGVhZGVyJyl9XHJcbiAgICAgICAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgICAgICkpfVxyXG4gICAgICAgICAgPC90cj5cclxuICAgICAgICApKX1cclxuICAgIDwvdGhlYWQ+XHJcbiAgICB7LyogQXBwbHkgdGhlIHRhYmxlIGJvZHkgcHJvcHMgKi99XHJcbiAgICA8dGJvZHkgey4uLmdldFRhYmxlQm9keVByb3BzKCl9PlxyXG4gICAgICB7Ly8gTG9vcCBvdmVyIHRoZSB0YWJsZSByb3dzXHJcbiAgICAgICAgcm93cy5tYXAocm93ID0+IHtcclxuICAgICAgICAgIC8vIFByZXBhcmUgdGhlIHJvdyBmb3IgZGlzcGxheVxyXG4gICAgICAgICAgcHJlcGFyZVJvdyhyb3cpXHJcbiAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAvLyBBcHBseSB0aGUgcm93IHByb3BzXHJcbiAgICAgICAgICAgIDx0ciB7Li4ucm93LmdldFJvd1Byb3BzKCl9PlxyXG4gICAgICAgICAgICAgIHsvLyBMb29wIG92ZXIgdGhlIHJvd3MgY2VsbHNcclxuICAgICAgICAgICAgICAgIHJvdy5jZWxscy5tYXAoY2VsbCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIC8vIEFwcGx5IHRoZSBjZWxsIHByb3BzXHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPHRkIHsuLi5jZWxsLmdldENlbGxQcm9wcygpfT5cclxuICAgICAgICAgICAgICAgICAgICAgIHsvLyBSZW5kZXIgdGhlIGNlbGwgY29udGVudHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2VsbC5yZW5kZXIoJ0NlbGwnKX1cclxuICAgICAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIClcclxuICAgICAgICB9KX1cclxuICAgIDwvdGJvZHk+XHJcbiAgPC90YWJsZT5cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0Um93U3R5bGUoZGF5T2ZXZWVrKSB7XHJcbiAgaWYgKGRheU9mV2VlayA9PSAxKSB7XHJcbiAgICByZXR1cm4gdGFibGVTdHlsZXMucm93X21vbmRheTtcclxuICB9IGVsc2UgaWYgKGRheU9mV2VlayA9PSAyKSB7XHJcbiAgICByZXR1cm4gdGFibGVTdHlsZXMucm93X3R1ZXNkYXk7XHJcbiAgfSBlbHNlIGlmIChkYXlPZldlZWsgPT0gMykge1xyXG4gICAgcmV0dXJuIHRhYmxlU3R5bGVzLnJvd193ZWRuZXNkYXk7XHJcbiAgfSBlbHNlIGlmIChkYXlPZldlZWsgPT0gNCkge1xyXG4gICAgcmV0dXJuIHRhYmxlU3R5bGVzLnJvd190aHVyc2RheTtcclxuICB9IGVsc2UgaWYgKGRheU9mV2VlayA9PSA1KSB7XHJcbiAgICByZXR1cm4gdGFibGVTdHlsZXMucm93X2ZyaWRheTtcclxuICB9IGVsc2UgaWYgKGRheU9mV2VlayA9PSA2KSB7XHJcbiAgICByZXR1cm4gdGFibGVTdHlsZXMucm93X3NhdHVyZGF5O1xyXG4gIH0gZWxzZSBpZiAoZGF5T2ZXZWVrID09IDcpIHtcclxuICAgIHJldHVybiB0YWJsZVN0eWxlcy5yb3dfc3VuZGF5O1xyXG4gIH1cclxufSJdLCJzb3VyY2VSb290IjoiIn0=